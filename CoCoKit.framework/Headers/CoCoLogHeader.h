//
//  CoCoLogHeader.h
//  CoCoKit
//
//  Created by 陈明 on 2018/4/26.
//  Copyright © 2018年 陈明. All rights reserved.
//

#ifndef CoCoLogHeader_h
#define CoCoLogHeader_h

#ifdef DEBUG
#define CoCoLog(FORMAT, ...) fprintf(stderr, "%s\n-----------------------------------------------\n", [[NSString stringWithFormat:FORMAT, ## __VA_ARGS__] UTF8String]);
#else
#define CoCoLog(...)
#endif

#define CoCoDeallocd CoCoLog(@"%@", [NSString stringWithFormat:@"♻️【 %@ 】已释放!", ClassName(self)]);


#endif /* CoCoLogHeader_h */
