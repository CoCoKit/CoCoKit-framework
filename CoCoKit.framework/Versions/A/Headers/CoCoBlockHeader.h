//
//  CoCoBlockHeader.h
//  CoCoKit
//
//  Created by 陈明 on 2018/4/26.
//  Copyright © 2018年 陈明. All rights reserved.
//

#ifndef CoCoBlockHeader_h
#define CoCoBlockHeader_h

#define CoCoWeakSelf(type)   __weak typeof(type)weak ## type = type;
#define CoCoStrongSelf(type) __strong typeof(type)type = weak ## type;
#define coco_weakSelf   CoCoWeakSelf(self)
#define coco_strongSelf CoCoStrongSelf(self)

#endif /* CoCoBlockHeader_h */
