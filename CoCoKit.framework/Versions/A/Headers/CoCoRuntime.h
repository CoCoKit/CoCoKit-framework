//
//  CoCoRuntime.h
//  CoCoKit
//
//  Created by 陈明 on 2018/5/19.
//  Copyright © 2018年 陈明. All rights reserved.
//

#import <Foundation/Foundation.h>


// 交换两个方法
extern inline BOOL coco_method_exchange(Class klass, SEL origSel, SEL altSel);
extern inline void coco_method_append(Class toClass, Class fromClass, SEL selector);
extern inline void coco_method_replace(Class toClass, Class fromClass, SEL selector);

@interface CoCoRuntime : NSObject

@end
