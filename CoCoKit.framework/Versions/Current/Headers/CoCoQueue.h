//
//  CoCoQueue.h
//  WeiboCoCo
//
//  Created by 陈明 on 2017/3/17.
//  Copyright © 2017年 com.weibococo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CoCoQueue : NSObject
+ (CoCoQueue *)Instance;

- (dispatch_queue_t)getQueue;

@end



static inline void runAsyncOnCoCoQueue(dispatch_block_t block){
    dispatch_async([[CoCoQueue Instance] getQueue], block);
}

static inline void runAsyncOnCoCoQueueAfter(dispatch_time_t delayInSeconds, dispatch_block_t block){
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC)), [[CoCoQueue Instance] getQueue], block);
}

static inline void runAsyncOnMainQueue(dispatch_block_t block){
    dispatch_async(dispatch_get_main_queue(), block);
}

static inline void runAsyncOnMainQueueAfter(dispatch_time_t delayInSeconds, dispatch_block_t block){
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), block);
}

static inline void runSyncOnMainQueue(dispatch_block_t block){
    dispatch_sync(dispatch_get_main_queue(), block);
}

static inline void runAsyncOnGlobalDefaultQueue(dispatch_block_t block){
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), block);
}
